<?php
namespace Veebipoed\LiisiContract\Controller\Index;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Registry;
use SimpleXMLElement;

class Index extends \Magento\Framework\App\Action\Action {

    protected $resultPageFactory;
    protected $_coreRegistry;

    /**
     * Constructor
     *
     * @param \Magento\Framework\App\Action\Context  $context
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Framework\Registry $coreRegistry
    )
    {
        $this->resultPageFactory = $resultPageFactory;
        $this->_coreRegistry = $coreRegistry;
        parent::__construct($context);
    }

    /**
     * Execute view action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $order_id = (int)(isset($_GET['order_id']) ? $_GET['order_id'] : (isset($_POST['order_id']) ? $_POST['order_id'] : 0));
        if (!$order_id) {
            $this->_redirect("/");
            return;
        }
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $order = $objectManager->create('Magento\Sales\Model\Order')->load($order_id);
        $orderData = $order->getBillingAddress()->toArray();
        $errors = array();
        if (isset($_POST['submitLiisiForm'])) {
            $errors = $this->validate();
            if (count($errors) == 0) {
                $xml = $this->createRequest($order);
                $resultXML = $this->sendRequest($xml);
                if ($resultXML->status == 1) {
                    $result = $this->processResult($resultXML->data, $order);
                } else {
                    $result = (object)array("code"=>"error", "comment"=>$resultXML->error, "terms"=>array());
                }
                $this->_coreRegistry->register("success", $result->code);
                $this->_coreRegistry->register("successComment", $result->comment);
                if (isset($result->terms)) {
                    $this->_coreRegistry->register("successTerms", $result->terms);
                }
            }
        }
        if (isset($_POST['confirmContract'])) {
            $confXML = $this->createConfirmationRequest($order_id);
            $this->_coreRegistry->register("success", "final_success");
            $result = $this->sendRequest($confXML);
            $order->setStatus("processing");
            $order->save();
        }
        $this->_coreRegistry->register('error', join("<br>", $errors));
        $this->_coreRegistry->register('firstname', $orderData['firstname']);
        $this->_coreRegistry->register('lastname', $orderData['lastname']);
        $this->_coreRegistry->register('company', $orderData['company']);
        $this->_coreRegistry->register('address_street', $orderData['street']);
        $this->_coreRegistry->register('address_region', $orderData['region']);
        $this->_coreRegistry->register('address_zip', $orderData['postcode']);
        $this->_coreRegistry->register('address_city', $orderData['city']);
        $this->_coreRegistry->register('email', $orderData['email']);
        $this->_coreRegistry->register('telephone', $orderData['telephone']);
        $this->_coreRegistry->register('order_id', $order_id);
        return $this->resultPageFactory->create();
    }

    public function validate() {
        $error = array();
        $postData = $_POST;
        if (strlen($postData['firstname']) < 2) $error[] = __('Eesnimi peab olema vähemalt 2 tähemärki pikk.');
        if (strlen($postData['lastname']) < 2) $error[] = __('Perekonnanimi peab olema vähemalt 2 tähemärki pikk.');
        if (strlen($postData['idcode']) < 11) $error[] = __('Isikukood peab olema vähemalt 11 tähemärki pikk.');
        if (strlen($postData['email']) < 3) $error[] = __('E-mail peab olema vähemalt 3 tähemärki pikk.');
        if (strlen($postData['phone']) < 5) $error[] = __('Telefoni number peab olema vähemalt 5 tähemärki pikk.');
        if (strlen($postData['employer']['name']) < 3) $error[] = __('Tööandja nimi peab olema vähemalt 3 tähemärki pikk.');
        return $error;
    }

    private function createConfirmationRequest($order_id) {
        $xml = new SimpleXMLElement('<ConfirmationRequest/>');
        $connection = $xml->addChild("Connection");
        $connection->addChild("Version", "4");
        $xml->addChild("OrderId", $order_id);
        return $xml->asXML();
    }

    public function createRequest($orderObj) {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $helper = $objectManager->create('Veebipoed\LiisiContract\Model\LiisiContract');
        $helper->getLiisiAPI();


        $post = $_POST;
        $xml = new SimpleXMLElement('<ApplicationRequest/>');
        $connection = $xml->addChild("Connection");
        $connection->addChild("Version", "4");
        $xml->addChild("OrderId", (int)$orderObj->getRealOrderId());
        $client = $xml->addChild("Client");
        $client->addChild("Identified", "no");
        $client->addChild("IdCode", $post['idcode']);
        $client->addChild("Surname", $post['lastname']);
        $client->addChild("FirstName", $post['firstname']);
        $client->addChild("Phone", $post['phone']);
        $client->addChild("Email", $post['email']);
        $address = $client->addChild("Address");
        $address->addChild("Street", $post['address']['street']);
        $address->addChild("HouseNr", $post['address']['house']);
        $address->addChild("Apartment", $post['address']['apartment']);
        $address->addChild("Locality", $post['address']['city']);
        $address->addChild("County", $post['address']['county']);
        $address->addChild("ZipCode", $post['address']['zip']);
        $politicallyExposed = (isset($post['politically_exposed']) ? (int)$post['politically_exposed'] : 0);
        $client->addChild("PoliticallyExposed", ($politicallyExposed ? "true" : "false"));
        $employment = $client->addChild("Employment");
        $pensionQuery = (isset($post['pension_query']) ? (int)$post['pension_query'] : 0);
        $employment->addChild("AllowPensionQuery", ($pensionQuery ? "true" : "false"));
        $employment->addChild("Employer", $post['employer']['name']);
        $employment->addChild("TimeWithEmployer", $post['employer']['work_time']);
        $employment->addChild("SalaryAmount", $post['employer']['salary']);
        $employment->addChild("LoanObligations", $post['employer']['loan']);
        $productList = $orderObj->getItems();
        $products = $xml->addChild("Products");
        foreach ((array)$productList as $p) {
            $quantity = (int)$p->getQtyOrdered();
            $productData = $p->getData();
            $product = $products->addChild("Product");
            $product->addChild("Name", $productData['name']);
            $product->addChild("Quantity", $quantity);
            $product->addChild("TotalPrice", round($productData['row_total'], 2));
            $product->addChild("ProductCode", $productData['sku']);
            $product->addChild("ProductId", $productData['product_id']);
        }

        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
        $return_url = $storeManager->getStore()->getBaseUrl()."liisicontract/index/index/";

        $payment = $xml->addChild("Payment");
        $payment->addChild("ScheduleDuration", $post['payment']['period']);
        $payment->addChild("DownPayment", round((float)$post['payment']['downpayment'], 2));
        $payment->addChild("PostalFee", round($orderObj->getShippingAmount(), 2));

        $reply = $xml->addChild("Reply");
        $reply->addChild("URL", $return_url);
        $reply->addChild("Username", "test_arendaja");
        $reply->addChild("Password", "arendaja");
        $xml->addChild("ClientIpAddress", $_SERVER['REMOTE_ADDR']);
        return $xml->asXML();
    }

    private function getSSLKey($certificate) {
        $currentPath = getcwd()."/";
        $realPath = $currentPath."tmp.".rand(0, 1000).".pem";
        file_put_contents($realPath, $certificate);
        return $realPath;
    }

    private function sendRequest($xml) {
        $result = (object)array("status"=>0, "error"=>"", "data"=>"");

        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $helper = $objectManager->create('Veebipoed\LiisiContract\Model\LiisiContract');
        $helper->getLiisiAPI();

        $username = $helper->username;
        $password = $helper->password;
        if ($helper->mode == "test") {
            $url = "https://prelive.liisi.ee:9300/webshop/application/";
        } else {
            $url = "https://pood.liisi.ee:9300/webshop/application/";
        }
        $ch = curl_init();
        $keyPath = $this->getSSLKey($helper->certificate);
        $options = array(
            CURLOPT_HTTPHEADER => array('Content-Type: application/xml', 'Connection: close'),
            CURLOPT_URL => $url,
            CURLOPT_SSLVERSION => 4,
            CURLOPT_SSLKEY => $keyPath,
            CURLOPT_SSLCERT => $keyPath,
            CURLOPT_USERPWD => $username . ':' . $password,
            CURLOPT_SSL_CIPHER_LIST => 'SSLv3',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_POST => true,
            CURLOPT_POSTFIELDS => $xml,
//          CURLOPT_SSL_VERIFYPEER => false
        );
        curl_setopt_array($ch, $options);
        $data = curl_exec($ch);
        if (!$data) {
            $result->error = curl_error($ch);
        } else {
            $result->status = 1;
            $result->data = $data;
        }
        unlink($keyPath);
        curl_close($ch);
        return $result;
    }

    private function processResult($data, $order) {
        $result = (object)array("code"=>"error", "comment"=>__('Tekkis süsteemi viga.'));
        if ($data) {
            $xml = new SimpleXMLElement($data);
            if ($xml->Status == "ERROR") {
                $order->setStatus("holded");
                $order->save();
                $result->code = "error";
                $result->terms = $xml->Terms;
                $result->comment = $xml->Comment;
                return $result;
            } elseif ($xml->Status == "PENDING") {
                $order->setStatus("pending");
                $order->save();
                $result->code = "pending";
                $result->terms = $xml->Terms;
                return $result;
            } elseif ($xml->Status == "AUTO_SATISFIED") {
                $order->setStatus("pending");
                $order->save();
                $result->code = "success";
                $result->terms = $xml->Terms;
                return $result;
            } elseif ($xml->Status == "SATISFIED") {
                $order->setStatus("pending");
                $order->save();
                $result->code = "success";
                $result->terms = $xml->Terms;
                return $result;
            } elseif ($xml->Status == "REJECTED") {
                $order->setStatus("canceled");
                $order->save();
                $result->code = "fail";
                $result->terms = $xml->Terms;
                $result->comment = $xml->Comment;
                return $result;
            }
        }
        $result->comment .= " '".$data."'";
        return $result;
    }

}
?>
