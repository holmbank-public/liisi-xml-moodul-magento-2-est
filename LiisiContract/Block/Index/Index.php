<?php
namespace Veebipoed\LiisiContract\Block\Index;
class Index extends \Magento\Framework\View\Element\Template
{
	protected $_coreRegistry;

	public function __construct(\Magento\Framework\View\Element\Template\Context $context, \Magento\Framework\Registry $coreRegistry)
	{
		$this->_coreRegistry = $coreRegistry;
		parent::__construct($context);
	}

	public function getFieldValue($key)
	{
		return $this->_coreRegistry->registry($key);
	}
}